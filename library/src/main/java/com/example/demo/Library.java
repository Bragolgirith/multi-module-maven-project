package com.example.demo;

public class Library {
    public static String greet(String subject) {
        return String.format("Hello, %s!", subject);
    }
}
